import socket
from _thread import *
import sys

class Server():

    def __init__(self, ip, port):
        self.ip = str(ip)
        self.port = int(port)

    def send(self, conn, message):
        conn.send(str.encode(message))

    def received(self, conn):
        return conn.recv(2048).decode()

    def run_server(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.s.bind((self.ip, self.port))
        except socket.error as e:
            print(str(e))
        self.s.listen(2)
        print("Waiting for a connection, Server Started")
        self.currentPlayer = []
        while True:
            conn, addr = self.s.accept()
            print("Connected to:", addr)

            start_new_thread(threaded_client, (conn, currentPlayer))
            self.currentPlayer.append([1, (conn, addr), {}])
            print(self.currentPlayer)

    def threaded_client(self, conn, player):
        conn.send(self.send(conn ,'oui'))
        reply = ""
        while True:
            try:
                data = read_pos(self.received())

                if not data:
                    print("Disconnected")
                    break
                else:
                    if player == 1:
                        reply = pos[0]
                    else:
                        reply = pos[1]

                    print("Received: ", data)
                    print("Sending : ", reply)

                conn.sendall(str.encode(make_pos(reply)))
            except:
                break

        print("Lost connection")
        conn.close()

if __name__ == '__main__':

    monServeur = Server('127.0.0.1', 5555)
    monServeur.run_server()