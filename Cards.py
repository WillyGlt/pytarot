class Card:
    def __init__(self, value, typeCard):
        self.value = value
        self.typeCard = typeCard
        self.mass = self.setMass()
        self.point = self.set_point()

    def set_point(self):
        if self.value == 'excuse':
            return 4.5
        elif self.value == 1:
            return 4.5
        elif self.value == 21:
            return 4.5
        elif self.value == 'roi':
            return 4.5
        elif self.value == 'dame':
            return 3.5
        elif self.value == 'cavalier':
            return 2.5
        elif self.value == 'valet':
            return 1.5
        else:
            return 0.5

    def __repr__(self):
        return '{0} {1} {2}'.format(self.typeCard, self.value, self.mass)

    def setMass(self):
        '''
        Définie la masse de la carte en fonction de sa valeur.
        '''
        if self.value == 'excuse':
            return 0
        if self.value == 'roi':
            return 1
        if self.value == 'dame':
            return 2
        if self.value == 'cavalier':
            return 3
        if self.value == 'valet':
            return 4
        if self.value == '21':
            return 5
        if self.value == '20':
            return 6
        if self.value == '19':
            return 7
        if self.value == '18':
            return 8
        if self.value == '17':
            return 9
        if self.value == '16':
            return 10
        if self.value == '15':
            return 11
        if self.value == '14':
            return 12
        if self.value == '13':
            return 13
        if self.value == '12':
            return 14
        if self.value == '11':
            return 15
        if self.value == '10':
            return 16
        if self.value == '9':
            return 17
        if self.value == '8':
            return 18
        if self.value == '7':
            return 19
        if self.value == '6':
            return 20
        if self.value == '5':
            return 21
        if self.value == '4':
            return 22
        if self.value == '3':
            return 23
        if self.value == '2':
            return 24
        if self.value == '1':
            return 25
