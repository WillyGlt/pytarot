import socket
from _thread import *
import sys
import threading
import time
import pickle
from Player import *
from package import *
import main

allThreads = {}
currentPlayers = {}
playerMax = 0
package = Package()
dog = Dog()

def setPlayerMax(nb):
    global playerMax, currentPlayers
    if playerMax == 0:
        playerMax = nb

class Thread(threading.Thread):
    def __init__(self, conn, addr):
        threading.Thread.__init__(self)
        self.conn = conn
        self.addr = addr
        self.name = self.getName()
        self.needAction = False
        self.messageAction = ""

    # Recois des données 
    def recv(self):
        return self.conn.recv(100)

    # Envoie de données
    def send(self, data):
        self.conn.send(pickle.dumps(data))

    # Fonction principale du thread
    def run(self):
        try:
            data = pickle.loads(self.recv())
            # Ajoute le clien à tous les Players stockés 
            currentPlayers[self.addr[1]] = Player(self.addr[1], data['username'])
            setPlayerMax(data['nb_player'])
            while True:
                # Envoie data au client
                message = {
                    'dataPlayer': currentPlayers[self.addr[1]],
                    'allPlayers': currentPlayers, 
                    'playerMax': playerMax,
                    'needAction': self.needAction, 
                    'actualPlayers': len(currentPlayers),
                }
                if self.needAction:
                    message['needMessage'] = self.messageAction
                # Evnoie données au clien
                self.send(message)
                if self.needAction:
                    data = pickle.loads(self.recv())
                    if data['for'] == 'contract':
                        currentPlayers[self.addr[1]].contractChoice = main.takeContract(currentPlayers[self.addr[1]], data['needMessage'])
                    self.needAction = False
                print('========', self.addr[1], len(currentPlayers[self.addr[1]].hand.cards))
                time.sleep(3)

        except Exception as e:
            print("===========================", e)
            self.quit()

    # Quitte de thread et supprime les données des joueurs
    def quit(self):
        print("Client arrêté. Connexion interrompue.")
        del currentPlayers[self.addr[1]]
        del allThreads[self.addr[1]]
        self.conn.close()

# Thread de jeu
class ThreadServer(threading.Thread):
    def run(self):
        global currentPlayers, allThreads
        while True:
            if playerMax != 0 and len(currentPlayers) == playerMax:
                package.giveCards(currentPlayers.values(), dog)
                while playerMax != 0 and len(currentPlayers) == playerMax:
                    # Demande le contrat à tous les joueurs
                    for k, thread in allThreads.items():
                        thread[1].needAction = True
                        thread[1].messageAction = f"Choisissez un contrat:\n1: petite 2: garde 3: gardeSans 4: gardeContre"
                    # Boucle de jeu principale
                    while True:
                        time.sleep(4)
                        print('suite..')
                    
    def sendAll(self, msg):
        global currentPlayers, allThreads
        for k, v in allThreads.items():
            v[0].send(pickle.dumps(msg))

class Server():
    def __init__(self, ip, port):
        self.ip = str(ip)
        self.port = int(port)

    def sendAll(self):
        for k, v in allThreads.items():
            v[0].send(pickle.dumps({'allPlayers': currentPlayers, 'data': 'ready'}))

    def send(self, conn, message):
        conn.send(str.encode(message))

    def received(self, conn):
        return conn.recv(2048).decode()

    def run_server(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setblocking(1)
        try:
            self.s.bind((self.ip, self.port))
        except socket.error as e:
            print(str(e))
        self.s.listen()
        print("Serveur démarré")
        TS = ThreadServer()
        TS.start()
        while True:
            conn, addr = self.s.accept()
            if len(currentPlayers) < playerMax or playerMax == 0:
                print(addr, ' Vient de se connecter!')
                allThreads[addr[1]] = [conn, Thread(conn, addr)]
                allThreads[addr[1]][1].start()

if __name__ == '__main__':
    monServeur = Server('localhost', 5555)
    monServeur.run_server()