import json


class ToFile:
    def __init__(self, file, type_of_file, new_value, json_key=None):
        if type_of_file.lower() == "json":
            final_data = {}
            with open(file, 'r') as work_file:
                json_data = json.load(work_file)
                if json_key == None:
                    pass
                else:
                    json_data[json_key] = new_value
                    final_data = json_data

            with open(file, 'w') as json_file:
                json_file.write(json.dumps(final_data))

        elif type_of_file.lower() == "log":
            with open(file, 'a') as log_file:
                log_file.write('{}\n'.format(new_value))


def ReadFile(file, type_of_file, key):
    if type_of_file.lower() == "json":
        with open(file, 'r') as work_file:
            json_data = json.load(work_file)
            return json_data[key]

    elif type_of_file.lower() == "log":
        with open(file, 'r') as log_file:
            return log_file.readlines()