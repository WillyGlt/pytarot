"""
Création d'un dictionnaire pour les contrats
"""
dict_contract = {
    'petite' : 25,
    'garde' : 25 * 2,
    'garde sans' : 25 * 3,
    'garde contre' : 25 * 5
}

"""
Création d'un dictionnaire pour le nombre points à réaliser celon le nombre de bouts (oudlers)
"""
dict_oudlers = { 
    0 : 56,
    1 : 51,
    2 : 41,
    3 : 36
}
"""
Création d'un dictionnaire pour les primes (bonus)
"""
liste_des_primes = []
dict_primes = {
    'petit au bout' : 10,
    'poignée simple' : 20,
    'poignée double' : 30,
    'poignée triple' : 40,
    'petit chelem annoncé réussi' : 200,
    'petit chelem annoncé échoué' : -100,
    'petit chelem non annoncé réussi' : 100,
    'grand chelem annoncé réussi' : 400,
    'grand chelem annoncé échoué' : -200,
    'grand chelem non annoncé réussi' : 200
}

"""
Création d'un dictionnaire pour stocker les jouers
"""
dico_joueurs = dict()

for i in range(0, nb_joueurs):
    joueur = input("Nom du joueur {} : ".format(i + 1))
    print(joueur)

    # Initialisation des scores à 0
    dico_joueurs[joueur] = [0]

"""
Gestion nb joueurs
"""
nb_joueurs = -1
while True:
    try:
        print('Entre 3 et 5 joueurs, svp.')
        nb_joueurs = int(input("Saisir le nombre de joueurs : "))
        if 2 < nb_joueurs < 6:
            # print("Nombre de joueurs : ", nb_joueurs)
            break
    except ValueError:
        print("Oops!  Réponse incorrecte, ce n'est pas un nombre... Réessayer...")

"""
Calcul des scores
"""

"""
def verificationContrat(nombre_de_points, pari_oudlers):
    # Contrat rempli ou non
    if nombre_de_points >= pari_oudlers:
        print("Le contrat est rempli.")
    else:
        print("Le contrat n'est pas rempli.")

    gain = nombre_de_points - pari_oudlers

    print("{} points remportés dans ce tour.".format(gain))
    return gain
"""

def updateScore(gain,  preneur,  liste_beneficiaire_prime):
    # Ajout des scores
    print(dico_primes)

    # Si contrat rempli
    if (gain > 0):
        for nom in dico_joueurs:
            score = dico_joueurs[nom][-1]

            if nom == preneur:
                score = score + \
                    (gain + dico_contrat[contrat_tour]) * (nb_joueurs - 1)
            else:
                score = score - (gain + dico_contrat[contrat_tour])
            dico_joueurs[nom].append(score)

    # Si contrat non rempli
    else:
        for nom in dico_joueurs:
            score = dico_joueurs[nom][-1]

            if nom == preneur:
                score = score + \
                    (gain - dico_contrat[contrat_tour]) * (nb_joueurs - 1)
            else:
                score = score - (gain - dico_contrat[contrat_tour])
            dico_joueurs[nom].append(score)

    # si primes
    print("Dico des primes : ", dico_primes)
    print("Liste des primes : ", liste_beneficiaire_prime)

    i = 0
    for nom in liste_beneficiaire_prime:
        print(nom)
        if nom[0] == liste_beneficiaire_prime[i][0]:
            bonus = dico_primes[liste_beneficiaire_prime[i][1]]
            print(nom, bonus)
        else:
            bonus = dico_primes[liste_beneficiaire_prime[i][1]] * -1
            print(nom, bonus)
        i += 1
    print(dico_joueurs)


"""
Initialisation des paramètres du tour / Gestion Contrat, Atout et Primes
"""


def parametrageTour():

    # Gestion du preneur
    preneur = ""
    while True:
        try:
            preneur = input("Preneur : ")
            if preneur in dico_joueurs:
                print("Le preneur est {}".format(preneur))
                break
            else:
                print("Jouer inexistant")
        except ValueError:
            print("Erreur...")
# Gestion contrat

    contrat_tour = ""
    while True:
       try:
            contrat_tour = input("Contrat : ")
            contrat_tour = contrat_tour.lower()
            if contrat_tour in dico_contrat:
                print("Le contrat est {}".format(contrat_tour))
                break
            else:
                print("Contrat inexistant")
        except ValueError:
            print("Erreur...")

        # Gestion nb atout
        nb_oudlers = ""
        while True:
            try:
                print('Nb Atouts : 0, 1, 2 ou 3')
                nb_oudlers = int(input("Saisir le nombre d'atouts : "))
                if nb_oudlers in dico_oudlers:
                    print(
                        "Nombre d'atout est {}. Il faut réaliser {} points.".format(nb_oudlers, dico_oudlers[nb_oudlers]))
                    break
            except ValueError:
                print("nb entre 0 et 3")

        # Gestion prime

    """
    Test
    """
    # while True:
        # nb_points, nb_oudlers, contrat_tour, preneur, liste_des_primes = parametrageTour()
        # gain = verificationContrat(nb_points, dico_oudlers[nb_oudlers])
        # updateScore(gain, preneur, liste_des_primes)
