import socket
import pickle
import threading

class Network:
    def __init__(self, ip, port):
        self.client = socket.socket()
        self.ip = str(ip)
        self.port = port
        self.addr = (self.ip, self.port)
        self.pos = self.connect()

    def getPos(self):
        return self.pos

    def connect(self):
        try:
            self.client.connect(self.addr)
        except socket.error as e:
            print(str(e))
            
    def send(self, data):
        try:
            print("====", data)
            self.client.send(pickle.dumps(data))
        except socket.error as e:
            print(e)

    def recv(self):
        return pickle.loads(self.client.recv(4096))


# class Thread(threading.Thread):
#     def run(self):
#         while True:
#             data = 