import random
try:
    from Cards import Card
except:
    pass
try:
    from Player import Player
except:
    pass


class Package:

    def __init__(self):
        """
        Initialise l'objet avec les valeurs / types des différentes cartes
        """
        self.value = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                      "valet", "cavalier", "dame", "roi"]
        self.typeCard = ["pique", "carreau", "trefle", "coeur"]
        self.atout = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
                      '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', "excuse"]
        self.package = self.create()

    def __repr__(self):
        """
        Affiche l'objet en retournant la variable package
        """
        return str(self.package)

    def create(self):
        # MAGIE OBSCURE : Pour mélanger notre paquet de cartes
        # à l'aide du random sample, on va tout d'abord prendre les valeurs des cartes (x) ainsi que leur type (y) (pique, carreau...) + idem pour les atouts pour un total de 78 cartes
        return random.sample([Card(x, y) for x, y in [(x, y) for x in self.value for y in self.typeCard]+[(x, "atout") for x in self.atout]], 78)

    def giveCards(self, listPlayers, dog):
        listPlayers = list(listPlayers)
        allCards = []
        for i in range(len(listPlayers)):
            allCards.append([])
        """
        docstring
        """
        if len(listPlayers) == 5:
            y = 26
            z = 3
        else:
            y = 25
            z = 6
        doggoRandom = self.dogRandom(listPlayers, y, z)
        for i in range(1, y):
            if len(self.package) == 0:
                print('PTDR JE LE SAVAIS')
            try:
                if i == doggoRandom[0]:
                    dog.hand.append(self.package.pop())
                    del doggoRandom[0]
            except:
                pass
            for u in range(1, 4):
                allCards[i % len(listPlayers)].append(self.package.pop())
        index = 0
        for i in allCards:
            listPlayers[index].hand.cards = i
            index += 1

    def dogRandom(self, listPlayers, y, z):
        doggoRandom = sorted(random.sample(range(2, y-1), z))
        for i in range(len(doggoRandom)):
            try:
                if doggoRandom[i]+1 == doggoRandom[i+1]:
                    return self.dogRandom(listPlayers, y, z)
            except:
                pass
        return doggoRandom


class Dog:
    def __init__(self):
        self.hand = []
