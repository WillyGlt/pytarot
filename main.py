from Player import *
from package import *

# Init des joueurs
p1 = Player(1, 'j1')
p2 = Player(2, 'j2')
p3 = Player(3, 'j3')
p4 = Player(4, 'j4')

d = Dog()  # Initialisation du Chien
pack = Package()  # Init du packet de jeu
pack.giveCards([p1, p2, p3, p4], d)  # Donne les cartes du packet au joueur
team = [[None], [None]]  # 2 équipes, attaquant et defensseurs

allPlayers = (p1, p2, p3, p4)
board = [[], '']
'''
petite < garde < gardeSans < gardeContre
'''
# Tableau qui contient :
#       - tableau des contrats possibles 
#       - un chiffre qui correspond au "poids du contrat" maximum séléctionné par un joueur
contracts = [['petite', 'garde', 'gardeSans', 'gardeContre'], -1] 
atoutBoard = [i for i in board[0] if 'atout' == i.typeCard] # Récupere tous les atouts dans le board (s'il y en a)

def takeContract(player, choice):
    '''
    Les joueurs choisissent un 'contrat' en début de partie

    Prendre le contrat en début de partie
    Demander a chaque joueurs avec une surenchère sur la derniere prise de contrat si il y en a une
    Mettre en paramettre une chaine de caractere vide, si le joeur ne choisit pas de "choice"
    '''
    # del lesContrats[indexwDuContrat] | Supprimer tous contrats qui precedes le contrat selectionné
    if choice != '':
        for c in contracts[0]:
            try:
                if choice == c and contracts[1] < contracts[0].index(c):
                    contracts[1] = contracts[0].index(c)
                    player.contractChoice = c
            except:
                print("ERROR: ", Exception)


def setCardOnBoard(player, indexCard, board):
    board[0].append([player.hand[indexCard-1], player])
    return player.hand[indexCard-1]


def playCard(player):
    '''
    Joueur joue une carte lors de son tour
    '''
    # MAGIE OBSCURE: Met les cartes de type 'atout' dans un tableau
    # Check le tableau board[0] si il comporte une carte de type 'atout' et remplis le tableau dans atoutBoard
    atoutBoard = [i for i in board[0] if 'atout' == i[0].typeCard]
    print('atout dans le board: ', atoutBoard)
    print(f'MAIN JOUEUR {player.name}: {player.hand}')
    # Joueur qui engage la partie
    if player == allPlayers[0]:
        print('First to play')
        inputCard = int(input())
        setCardOnBoard(player, inputCard, board)
        board[1] = player.hand[inputCard-1].typeCard
        player.hand.pop(inputCard-1)
    # 1 si le joueur a la couleur, 2 si il ne l'a pas mais a un atout, 0 si il n'a ni la couleur ni un atout
    elif player != allPlayers[0] and player.ifTypeCard(board[1]) == 1:
        print('not First but color')
        getCard = None
        while getCard != board[1]:
            inputCard = int(input())
            getCard = player.hand[inputCard-1].typeCard
            if getCard != board[1]:
                print('Arrete de tricher frere, pause la meme couleur!')
        setCardOnBoard(player, inputCard, board).typeCard
        player.hand.pop(inputCard-1)
    elif player != allPlayers[0] and player.ifTypeCard(board[1]) == 2:
        print('not First but atout')
        # pas d'atout sur le board ou atout inférieur au plus fort deja sur le board
        if len(atoutBoard) == 0 or min([i[0].mass for i in atoutBoard]) < min([i.mass for i in player.hand if i.typeCard == 'atout']):
            print('pas d atout sur board ou atout inférieur')
            getCard = None
            while getCard != 'atout':
                inputCard = int(input())
                getCard = player.hand[inputCard-1].typeCard
                if getCard != 'atout':
                    print('Arrete de tricher frere, pose un atout')
            setCardOnBoard(player, inputCard, board).typeCard
            player.hand.pop(inputCard-1)

        # atout supérieur au plus fort deja sur board
        elif min([i[0].mass for i in atoutBoard]) > min([i.mass for i in player.hand if i.typeCard == 'atout']):
            print('atout supérieur dans la main')
            getCard = Card(0, 'try')
            while True:
                inputCard = int(input())
                getCard = player.hand[inputCard-1]
                if getCard.typeCard == 'atout' and min([i[0].mass for i in atoutBoard]) > getCard.mass:
                    break
                else:
                    print('Arrete de tricher frere, pose un atout plus fort')
            setCardOnBoard(player, inputCard, board)
            player.hand.pop(inputCard-1)
    # pas d'atout ni de carte correspondant au type de cartes present sur le board
    elif player != allPlayers[0] and player.ifTypeCard(board[1]) == 0:
        print('not First but nada')
        inputCard = int(input())
        setCardOnBoard(player, inputCard, board).typeCard
        player.hand.pop(inputCard-1)


def winRound():
    '''
    Définis le joueur qui à gagné la partie

    if len(atoutBoard) == 0:
        Valeur la plus forte de la premiere couleur joué gagne le tour
        recup la couleur
        comparé les chiffres de la meme couleur entre eux
        retourner le joueur qui a la valeure la plus forte
    else:
        recup les atouts
        comparé les chiffres des atout entre eux
        retourner le jouer qui a la valeur la plus forte

    Joueur qui a gagné le tour devient le premier joueur a jouer le prochain tour

    '''
    # Check si des atouts sont dans le board
    atoutBoard = [i for i in board[0] if 'atout' == i[0].typeCard]
    if len(atoutBoard) == 0:    # S'il y a pas d'atouts
        # Recupere la couleur du round
        couleur = tuple(i for i in board[0] if board[1] == i[0].typeCard)
        # Recupere la carte la plus forte par rapport à la couleur du round
        cardWin = min(tuple(i[0].mass for i in couleur))
        for c in couleur:
            if c[0].mass == cardWin:
                for card in board[0]:
                    c[1].cardsWin.append(card)
                return c[1]  # Retourne le joueur qui a posé cette carte

    else:  # Si pas d'atouts
        # Recupere la carte la plus forte par rapport à la couleurs du round
        cardWin = min(tuple(int(i[0].mass) for i in atoutBoard))
        for c in atoutBoard:
            if c[0].mass == cardWin:
                for card in board[0]:
                    c[1].cardsWin.append(card)
                return c[1]


def winContract():
    '''
    Définis si le contrat choisie en début de partie est réalisé

    Qd tous les tours joué, appel de la fonction ScoreCalculation (pour voir si le joueurs qui a pris le contrat la rempli ou non)
    '''



def winGame():
    '''
    === Récapitulatif des scores qd le nombre de manches (qu'on choisira genre 5 ou 10) sera atteint ===
    '''
    t1 = 0
    t2 = 0
    for alliance in team:
        for p in alliance:
            if alliance in team[0]:
                t1 += sum([i[0].point for i in p.cardsWin])                
            else:
                t2 += sum([i[0].point for i in p.cardsWin])
            
# team = [[p1,p2], [p3,p4]]
# takeContract(p2, 'petite')
# print(contracts[1])
# takeContract(p3, "gardeContre")
# print(contracts[1])
# playCard(p1)
# playCard(p4)
# print(winRound())
# board = [[], ''] # Pas oublié de réinitialiser Board apres un round
# playCard(p1)
# playCard(p4)
# print(winRound())
# winGame()