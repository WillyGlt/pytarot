import Contract
import Hand
import Cards


class Player:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.hand = Hand.Hand()
        self.points = 0
        self.isTurn = False
        self.winTrick = []
        self.contractChoice = Contract.Contract()
        self.cardsWin = []

    def sort_hand(self):
        """
        Call the function sort_hand() from the Hand class.
        """
        self.hand.sort_hand()

    def ifTypeCard(self, color):
        atout = 0
        for c in self.hand.cards:
            if c.typeCard == color and color != 'atout':
                return 1
            if c.typeCard == 'atout':
                atout = 1
        if atout == 1:
            return 2
        else:
            return 0

    def __repr__(self):
        return f'{self.name}'


# if __name__ == '__main__':
#     p1 = Player('1', 'Joueur 1')
#     print(f"""
#     Setup:
#     self.id = {p1.id}
#     self.name = {p1.name}
#     self.hand = {p1.hand.cards}
#     self.points = {p1.points}
#     self.isTurn = {p1.isTurn}
#     self.winTrick = {p1.winTrick}
#     self.contractChoice = {p1.contractChoice.name}
#     self.cardsWin = {p1.cardsWin}
#     """)
#
#     pic1 = Cards.Card('1', 'pique')
#     carcav = Cards.Card('cavalier', 'carreau')
#     car3 = Cards.Card('3', 'carreau')
#     piccave = Cards.Card('cavalier', 'pique')
#     picdame = Cards.Card('dame', 'pique')
#     pic2 = Cards.Card('2', 'pique')
#     trecav = Cards.Card('cavalier', 'trefle')
#     ato20 = Cards.Card('20', 'atout')
#     car1 = Cards.Card('1', 'carreau')
#
#     p1.hand = Hand.Hand(
#         [pic1,
#          carcav,
#          car3,
#          pic2,
#          picdame,
#          trecav,
#          ato20,
#          car1,
#          piccave]
#     )
#     p1.contractChoice = Contract.Contract('garde')
#
#     print('Main de {0}:\n{1}'.format(p1.name, p1.hand.cards))
#     p1.hand.sort_hand()
#     print('Main trié de {0}:\n{1}'.format(p1.name, p1.hand.cards))
#
#
#     print(f"""
#     Setup:
#     self.id = {p1.id}
#     self.name = {p1.name}
#     self.hand = {p1.hand.cards}
#     self.points = {p1.points}
#     self.isTurn = {p1.isTurn}
#     self.winTrick = {p1.winTrick}
#     self.contractChoice = {p1.contractChoice.name}
#     self.cardsWin = {p1.cardsWin}
#     """)
#
#     pic1 = Cards.Card('1', 'pique')
#     carcav = Cards.Card('cavalier', 'carreau')
#     car3 = Cards.Card('3', 'carreau')
#     piccave = Cards.Card('cavalier', 'pique')
#     picdame = Cards.Card('dame', 'pique')
#     pic2 = Cards.Card('2', 'pique')
#     trecav = Cards.Card('cavalier', 'trefle')
#     ato20 = Cards.Card('20', 'atout')
#     car1 = Cards.Card('1', 'carreau')