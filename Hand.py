from Cards import Card

# trefle -> pentacles/coins
# pique -> sword
# coeur -> cup
# carreau -> wood

# 0: 'atout'
# 1: 'pique'
# 2: 'coeur'
# 3: 'trefle'
# 4: 'carreau'


class Hand:
    def __init__(self, cards=[]):
        self.cards = cards

    def get_key(self, dic, val):
        keys = list(dic.keys())
        values = list(dic.values())
        return keys[values.index(val)]

    def sort_hand(self):
        atout_list = []
        pique_list = []
        coeur_list = []
        trefle_list = []
        carreau_list = []

        for iterator in self.cards:
            if iterator.typeCard == 'atout':
                atout_list.append(iterator)
            if iterator.typeCard == 'pique':
                pique_list.append(iterator)
            if iterator.typeCard == 'coeur':
                coeur_list.append(iterator)
            if iterator.typeCard == 'trefle':
                trefle_list.append(iterator)
            if iterator.typeCard == 'carreau':
                carreau_list.append(iterator)

        sorted(atout_list, key=lambda card: card.mass)
        sorted(pique_list, key=lambda card: card.mass)
        sorted(coeur_list, key=lambda card: card.mass)
        sorted(trefle_list, key=lambda card: card.mass)
        sorted(carreau_list, key=lambda card: card.mass)

        self.cards = atout_list + pique_list + coeur_list + trefle_list + carreau_list


# if __name__ == '__main__':
#     pic1 = Card('1', 'pique')
#     carcav = Card('cavalier', 'carreau')
#     car3 = Card('3', 'carreau')
#     piccave = Card('cavalier', 'pique')
#     picdame = Card('dame', 'pique')
#     pic2 = Card('2', 'pique')
#     trecav = Card('cavalier', 'trefle')
#     ato20 = Card('20', 'atout')
#     car1 = Card('1', 'carreau')
#
#     p1_hand = Hand(
#         [pic1,
#          carcav,
#          car3,
#          pic2,
#          picdame,
#          trecav,
#          ato20,
#          car1,
#          piccave]
#     )
#     print("Voici la liste initial:", p1_hand.cards)
#     p1_hand.sort_hand()
#     print("Voici la liste finale:", p1_hand.cards)
