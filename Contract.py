class Contract:
    def __init__(self, name='Nothing'):
        self.name = name
        self.target_points = 56
        self.is_win = None
        self.applied_points = 0

    def set_target_p(self, cards_wins):
        oudler_count = 0
        for card in cards_wins:
            if card.typeCard == 'atout' and card.value == '1':
                oudler_count += 1
            if card.typeCard == 'atout' and card.value == '21':
                oudler_count += 1
            if card.typeCard == 'atout' and card.value == 'excuse':
                oudler_count += 1
            else:
                pass
        if oudler_count == 1:
            self.target_points = 51
        if oudler_count == 2:
            self.target_points = 41
        if oudler_count == 3:
            self.target_points = 36
        else:
            self.target_points = 56

    def verify_win(self, player_points):
        if player_points >= self.target_points:
            self.is_win = True
        else:
            self.is_win = False

    def apply_contracts(self):
        # Default
        if self.is_win:
            self.applied_points = 25
        if not self.is_win:
            self.applied_points = -25
        # Garde: If is win, set the points x 2, else set the points x -2
        if self.is_win and self.name == 'garde':
            self.applied_points = self.target_points * 2
        if not self.is_win and self.name == 'garde':
            self.applied_points = self.target_points * -2
        # Garde sans: If is win, set the points x 4, else set the points x -4
        if self.is_win and self.name == 'garde_sans':
            self.applied_points = self.target_points * 4
        if not self.is_win and self.name == 'garde_sans':
            self.applied_points = self.target_points * -4
        # Garde contre: If is win, set the points x 6, else set the points x -6
        if self.is_win and self.name == 'garde_contre':
            self.applied_points = self.target_points * 6
        if not self.is_win and self.name == 'garde_contre':
            self.applied_points = self.target_points * -6
