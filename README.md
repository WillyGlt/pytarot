# Ydays - Algorythmie - Jeux de tarot

## Tarot à 3/4:

### Pour jouer au Tarot, il vous faut :

Le jeu de cartes de Tarot est composé de 78 cartes.
Être 4 joueurs (traditionnellement).

### Répartition des cartes du Tarot :

Un jeu de tarot est composé de 56 cartes d’une valeur de l’As au Roi comme un jeu traditionnel (trèfle, cœur, carreau, pique). La particularité réside dans le fait qu’une carte supplémentaire « Le cavalier » se situe entre le valet et la dame.

Les atouts sont quand à eux au nombre de 21 (de 1 à 21). Ils permettent lors de la partie de couper n’importe quelle couleur pour remporter le pli, sachant que leur valeur correspond à leur nombre.

La dernière carte est L’excuse et fonctionne un peu comme un joker. Elle a une grande valeur pour le décomptage de fin de partie mais ne permet pas de remporter un pli.
Le but du jeu du tarot :

Le principe du jeu du tarot est basé sur le pari de réussir un certain nombre de points pour le joueur qui prend. Pour cela il peut se faire aider par les Oudlers. Plus le joueur qui a prit en possède à la fin de partie, moins il devra faire de points pour réussir son pari.

    - Aucun Oudlers , il faut qu’il fasse 56 points
    - Avec 1 Oudlers, il faut qu’il fasse 51 points
    - Avec 2 Oudlers, il faut qu’il fasse 41 points
    - Avec 3 Oudlers, il faut qu’il fasse 36 points.

### Les Oudlers :

Les oudlers sont au nombre de 3 :

    - Le 21 d’atout. C’est la carte la plus forte du jeu.
    - Le 1 d’atout appelé également « Le petit ». Carte vulnérable, il faut la conserver étant donné sa valeur.
    - L’excuse. Carte imprenable, il faut la jouer judicieusement pour laisser filer un pli sans importance.

Chaque Oudler vaut d’office 5 points. Voilà pourquoi si vous possédez les 3, vous avez déjà 15 points et seulement 21 points (36 – 15)  à faire pour gagner.

### Commencer une partie de Tarot :

Pour commencer, le jeu est étalé sur le tapis face cachée et chaque joueur pioche une carte. Le joueur ayant la carte la plus faible est désigné pour distribuer.

Celui-ci distribue alors à chaque joueur les cartes 3 par 3. Le donneur doit également constituer « Le chien » composé de 6 cartes qu’il doit poser carte par carte sans que celle-ci ne soit la première et le dernière carte distribuée.

Chaque joueur retourne alors ses cartes sans les classer et celui situé à la gauche du donneur annonce s'il prend. Si la réponse est non, c’est au joueur situé après lui de parler. Un joueur peut supplanter un autre joueur en prenant un pari plus fort.

### Les paris (ou enchères) du Tarot :

Lorsque que l’un des joueurs se décide à prendre, il doit parier sa victoire en prenant plus ou moins de risques selon le niveau d’enchères choisit. A savoir que tous paris vaut 25 points d’office.

    - Une petite. Le pari ne rapporte rien de plus que les 25 points.
    - Une garde. Le pari est multiplié par 2 en cas de victoire ou défaite.
    - Une garde sans le chien (celui qui prend ne regarde pas le chien et le garde). Le pari est multiplié par 4 en cas de victoire ou défaite.
    - Une garde contre le chien (le chien revient à l’équipe). Le pari est multiplié par 6 en cas de victoire ou défaite.

Si personne ne décide de prendre, le donneur redistribue alors les cartes.

### Le déroulement de la partie :

A l’issue de la prise de pari d’un des joueurs, le chien composé de 6 cartes est alors retourné à la vue de tous (dans la cas d’une petite ou d’une garde) et donné au preneur. Celui-ci choisit 6 cartes de son choix (sauf le roi et les oudlers) et les repose devant lui sans les montrer. Les 3 autres joueurs forment alors pour toute la durée de cette partie une équipe devant tenter d’empêcher au preneur de réussir son pari.

C’est toujours le joueur situé à gauche du donneur qui commence la partie. Il joue une couleur et les autres joueurs ont l’obligation de fournir la couleur demandée si, bien entendu, ils en possèdent les cartes. S'ils n’en possèdent pas, ils peuvent couper à l’atout. Le gagnant du pli est celui qui ouvre le tour suivant.

Concernant les atouts, les joueurs ont l’obligation de monter à l’atout, c’est-à-dire de jouer une carte d’atout supérieure à celle précédemment jouée. Si un joueur ne possède plus d’atouts, ni de cartes de la couleur demandée, il peut poser n’importe quelle carte de son jeu.

L’excuse est toujours conservée par le joueur qui la possède. Le joueur ne peut remporter le pli puisqu’elle n’a pas de valeur directe lorsqu’elle est jouée. Celui-ci doit alors lorsque le tour est finit donner une carte sans valeur qu’il choisit en échange dans les précédentes levées pour compléter le pli. L’excuse ne peut être jouée au dernier tour sous peine de la perdre.

### Comment compter les points au Tarot ?

Pour chaque partie, il faut compter séparément le total des plis du preneur et les plis de l’équipe. Le total doit toujours donner 91 points.

Pour compter les cartes, il faut toujours joindre une carte basse avec une carte habillée.

Voyons maintenant la valeur des cartes :

    - Les 3 Oudlers valent chacun 5 points
    - Le roi vaut 5 points
    - La dame vaut 4 points
    - le cavalier vaut 3 points
    - Le valet vaut 2 points.
    - Les autres cartes par 2 valent 1 point.

Le preneur pour gagner doit donc faire au choix selon le nombre de Oudlers qu’il possède :

    - 56 points
    - 51 points
    - 41 points
    - 36 points

Si le pari du preneur est réussi, il prend immédiatement 25 points + le nombre de points fait en plus de son pari.

Exemple : Le preneur à pris une garde et doit faire 36 points. Son total de points s’élève à 41 points. Le résultat est donc 30 points (25 points + 5 points). Ayant pris à Garde le total est multiplié par 2. Donc le résultat est 60 points.

On met alors -60 points à chaque joueur de l’équipe et l’addition des 3 au preneur : + 180 points.

Ainsi, à la fin de la manche, le score total des joueurs doit toujours être égal à 0.

### Coups spéciaux :

*Le petit au bout*

Si le petit est amené jusqu’au dernier tour sans être perdu, il rapporte 10 points de plus à l’équipe ou le preneur qui le gagne et est multipliable selon l’enchère choisie.

*La poignée*

explication..................................................

Le type de poignée annoncé rapporte en prime des points supplémentaire :

    + 20 points pour la simple poignée (10 atouts)
    + 30 points pour la double poignée (13 atouts)
    – 40 points spour la triple poignée (15 atouts)

*Le grand chelem*

Pour réaliser un grand chelem, il faut remporter tous les plis. Il peut être annoncé après que le preneur est regardé le chien. Dans ce cas ce figure, c’est le preneur qui commence la partie et peut gagner le dernière pli avec l’excuse.

Le grand chelem rapporte en prime de points supplémentaire :

    + 400 points si le preneur l’annonce et le réalise
    + 200 points si le preneur ne l’annonce pas mais le réalise
    – 200 points si le preneur l’annonce mais ne le réalise pas.

*Le petit Chelem*

Certains joueurs ont incorporé cette règle au jeu du tarot. La différence avec le grand chelem réside dans le fait que le petit chelem est réalisé si le preneur ne laisse qu’un seul pli à ses adersaires.

Le petit chelem rapporte en prime de points supplémentaire :

    + 200 points si le preneur l’annonce et le réalise
    + 100 points si le preneur ne l’annonce pas mais le réalise
    – 100 points si le preneur l’annonce mais ne le réalise pas.
