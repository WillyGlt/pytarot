import requests
import shutil

for i in range(78):
    image_url = "https://gfx.tarot.com/images/site/decks/golden-thread/full_size/{}.jpg".format(i)
    filename = image_url.split("/")[-1]

    req = requests.get(image_url, stream=True)

    if req.status_code == 200:
        req.raw.decode_content = True

        with open(filename, 'wb') as f:
            shutil.copyfileobj(req.raw, f)

            print('Image sucessfully Downloaded: ',filename)
    else:
        print('Image Couldn\'t be retreived')