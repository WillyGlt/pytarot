import os
sort_nb_atouts = {
    0: 'excuse',
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    9: 9,
    10: 10,
    11: 11,
    12: 12,
    13: 13,
    14: 14,
    15: 15,
    16: 16,
    17: 17,
    18: 18,
    19: 19,
    20: 20,
    21: 21,
}

sort_nb = {
    14: 'roi',
    13: 'dame',
    12: 'cavalier',
    11: 'valet',
    10: '10',
    9: '9',
    8: '8',
    7: '7',
    6: '6',
    5: '5',
    4: '4',
    3: '3',
    2: '2',
    1: '1'
}
color = ''

range(22, 1, -1)
for i in range(22):
    color = 'atout'
    os.rename(r'./{}.jpg'.format(i), r'./{}-{}.jpg'.format(color, sort_nb_atouts[i]))
    print("file renamed: ./{}-{}.jpg".format(sort_nb_atouts[i], color))

for j in range(22, 36, 1):
    color = 'carreau'
    os.rename(r'./{}.jpg'.format(j), r'./{}-{}.jpg'.format(color, sort_nb[j-21]))
    print("file renamed: ./{}-{}.jpg".format(sort_nb[j-21], color))

for k in range(36, 50, 1):
    color = 'coeur'
    os.rename(r'./{}.jpg'.format(k), r'./{}-{}.jpg'.format(color, sort_nb[k-35]))
    print("file renamed: ./{}-{}.jpg".format(sort_nb[k-35], color))

for l in range(50, 64, 1):
    color = 'pique'
    os.rename(r'./{}.jpg'.format(l), r'./{}-{}.jpg'.format(color, sort_nb[l-49]))
    print("file renamed: ./{}-{}.jpg".format(sort_nb[l-49], color))

for m in range(64, 78, 1):
    color = "trefle"
    os.rename(r'./{}.jpg'.format(m), r'./{}-{}.jpg'.format(color, sort_nb[m-63]))
    print("file renamed: ./{}-{}.jpg".format(sort_nb[m-63], color))