import Player
import os


class Menu():
    def __init__(self):
        self.nb_players = -1
        self.players_list = []

    def say_hello(self):
        tarot_text = """
        
████████  █████  ██████   ██████  ████████     ███████ ███    ██     ██      ██  ██████  ███    ██ ███████ 
   ██    ██   ██ ██   ██ ██    ██    ██        ██      ████   ██     ██      ██ ██       ████   ██ ██      
   ██    ███████ ██████  ██    ██    ██        █████   ██ ██  ██     ██      ██ ██   ███ ██ ██  ██ █████   
   ██    ██   ██ ██   ██ ██    ██    ██        ██      ██  ██ ██     ██      ██ ██    ██ ██  ██ ██ ██      
   ██    ██   ██ ██   ██  ██████     ██        ███████ ██   ████     ███████ ██  ██████  ██   ████ ███████ 
        
        """

        print(tarot_text)

    def read_rules(self):
        os.system("less ./assets/regles.txt")

    def ask_action(self):
        term_with = os.get_terminal_size()[0]//2
        print("Que voulez-vous faire ?".center(term_with, ' '))
        print("[1] Regles".center(term_with, ' '))
        print("[2] Jouer".center(term_with, ' '))
        action = input("> ")
        while action != "1" and action != "2":
            print("Que voulez-vous faire ?".center(term_with, ' '))
            print("[1] Regles".center(term_with, ' '))
            print("[2] Jouer".center(term_with, ' '))
            if not action.isdigit():
                print(" Ce n'est pas un chiffre! ".center(term_with, '#'))
                action = input("> ")
            else:
                print(" Entrez un numéro valide ".center(term_with, '#'))
                action = input("> ")
        return action

    def ask_players(self):
        while int(self.nb_players) < 3 or int(self.nb_players) > 5:
            self.nb_players = input("Combien de joueur êtes-vous (de 3 à 5):\n")
            if not self.nb_players.isdigit():
                print("Entrez quelque chose de valide !")
                self.nb_players = input("Combien de joueur êtes-vous (de 3 à 5):\n")

        for the_id in range(int(self.nb_players)):
            self.players_list.append(player.Player(the_id, input("Entrez le nom du joueur n°{}:\n".format(the_id + 1))))

        return self.players_list


if __name__ == '__main__':
    the_menu = Menu()
    the_menu.say_hello()
    action = the_menu.ask_action()
    if action == "1":
        the_menu.read_rules()
    if action == "2":
        list_of_players = the_menu.ask_players()
        print(list_of_players)
