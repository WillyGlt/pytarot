# from pytarot.server.Network import Network
from socket import sethostname
import tkinter
from tkinter.constants import Y
import tkinter.messagebox
from typing import Text
from PIL import Image, ImageTk
from functools import partial
import Network
import pickle
import Player
from ToFile import *
import package

WINDOWS_WIDTH = 1200
WINDOWS_HEIGHT = 800
CASE_WIDTH = 600
CASE_WIDTH_MIDDLE = CASE_WIDTH/2
CASE_HEIGHT = 250
CASE_HEIGHT_MIDDLE = CASE_HEIGHT/2

local_player = Player.Player()
global_nb_player = 0


class GlobalApp(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        self.geometry("{0}x{1}".format(WINDOWS_WIDTH, WINDOWS_HEIGHT))
        self._frame = None
        self.switch_frame(StartPage)

    def switch_frame(self, frame_class):
        print("change de frame")
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()


class StartPage(tkinter.Frame):
    def __init__(self, master):
        print("Démarrage")
        tkinter.Frame.__init__(self, master)
        load = Image.open("assets/images/title.png")
        render = ImageTk.PhotoImage(load)
        img = tkinter.Label(self, image=render, relief=tkinter.FLAT)
        img.image = render
        # img.place(x=WINDOWS_WIDTH / 2 - 100, y=WINDOWS_HEIGHT / 2.5 - 50)
        img.pack(side=tkinter.TOP)

        go_button = tkinter.Button(self, text="Commencer", command=lambda: master.switch_frame(ConnectionForm), width=20,
                                   relief=tkinter.FLAT)
        go_button.pack()
        # go_button.place(x=WINDOWS_WIDTH / 2 - 100, y=WINDOWS_HEIGHT / 2)

        rules_button = tkinter.Button(self, text="Règles", command=lambda: master.switch_frame(Rules), width=20,
                                      relief=tkinter.FLAT)
        rules_button.pack()
        # rules_button.place(x=WINDOWS_WIDTH / 2 - 100, y=WINDOWS_HEIGHT / 2 + 40)

        exit_button = tkinter.Button(self, text="Quitter", command=lambda: master.switch_frame(QuitGame), width=20,
                                     relief=tkinter.FLAT)
        exit_button.pack()
        # exit_button.place(x=WINDOWS_WIDTH / 2 - 100, y=WINDOWS_HEIGHT / 2 + 80)
        self.pack()
        self.place(anchor=tkinter.S)


class Game(tkinter.Frame):
    def __init__(self, master):
        print("lunch_game")
        tkinter.Frame.__init__(self, master)
        # self.master = self.master
        self.configure(bg="#339966")
        self.pack(fill=tkinter.BOTH, expand=1)
        nb_player = ReadFile("assets/config.json", "json", "nb_users_asked")
        player_name = ReadFile("assets/config.json", "json", "local_username")

        p_one = Player.Player()
        p_one.name = player_name

        p_two = Player.Player()
        p_two.name = "Djoni"

        p_three = Player.Player()
        p_three.name = "Vanish"

        p_four = Player.Player()
        p_four.name = "Didier"


        list_of_player = []

        the_package = package.Package()
        the_package.create()

        for player_num in range(1, nb_player+1, 1):
            if len(range(nb_player)) == 3:
                list_of_player = [p_one, p_two, p_three]
            elif len(range(nb_player)) == 4:
                list_of_player = [p_one, p_two, p_three, p_four]

            # For player 1
            if player_num == 1:

                player1_frame = tkinter.Frame(self, borderwidth=2, relief=tkinter.GROOVE)
                player1_frame.place(x=WINDOWS_WIDTH/2 - CASE_WIDTH_MIDDLE,
                                    y=WINDOWS_HEIGHT - CASE_HEIGHT,
                                    width=CASE_WIDTH,
                                    height=CASE_HEIGHT)
                player1_lab = tkinter.Label(player1_frame, text=p_one.name)
                player1_lab.pack()

            # For player 2
            if player_num == 2:
                player2_frame = tkinter.Frame(self, borderwidth=2, relief=tkinter.GROOVE)
                player2_frame.place(x=WINDOWS_WIDTH/2 - CASE_WIDTH_MIDDLE,
                                    y=0,
                                    width=CASE_WIDTH,
                                    height=CASE_HEIGHT)
                player2_lab = tkinter.Label(player2_frame, text=p_two.name)
                player2_lab.pack()


            # For player 3
            if player_num == 3:
                player3_frame = tkinter.Frame(self, borderwidth=2, relief=tkinter.GROOVE)
                player3_frame.place(x=0,
                                    y=WINDOWS_HEIGHT/2 - CASE_WIDTH_MIDDLE,
                                    width=CASE_HEIGHT,
                                    height=CASE_WIDTH)
                player3_lab = tkinter.Label(player3_frame, text=p_three.name)
                player3_lab.pack()

            # For player 4
            if player_num == 4:
                player4_frame = tkinter.Frame(self, borderwidth=2, relief=tkinter.GROOVE)
                player4_frame.place(x=WINDOWS_WIDTH-CASE_HEIGHT,
                                    y=WINDOWS_HEIGHT/2 - CASE_WIDTH_MIDDLE,
                                    width=CASE_HEIGHT,
                                    height=CASE_WIDTH)
                player4_lab = tkinter.Label(player4_frame, text=p_four.name)
                player4_lab.pack()


class ConnectionForm(tkinter.Frame):
    def __init__(self, master):
        print("Inscription")
        tkinter.Frame.__init__(self, master)

        # Ask Username
        username_lab = tkinter.Label(master, text="Username", width=20)
        username = tkinter.StringVar()
        username_ent = tkinter.Entry(master, textvariable=username)

        # Ask Number of player you want
        nb_player_lab = tkinter.Label(master, text="Avec combien de joueurs voulez-vous jouer ?", width=33)
        nb_player = tkinter.IntVar()
        nb_player_ent_3 = tkinter.Radiobutton(master, text=3, value=3, variable=nb_player)
        nb_player_ent_4 = tkinter.Radiobutton(master, text=4, value=4, variable=nb_player)
        # nb_player_ent_5 = tkinter.Radiobutton(master, text=5, value=5, variable=nb_player)

        setup_infos = partial(self.setup_infos, username, nb_player)

        go_button = tkinter.Button(self,
        text="Commencer",
        command=setup_infos,
        width=20,
        relief=tkinter.FLAT)

        
        # Placement
        username_lab.pack(anchor=tkinter.CENTER)
        username_ent.pack(anchor=tkinter.CENTER)
        nb_player_lab.pack(anchor=tkinter.CENTER)
        nb_player_ent_3.pack(anchor=tkinter.CENTER)
        nb_player_ent_4.pack(anchor=tkinter.CENTER)
        # nb_player_ent_5.pack(anchor=tkinter.CENTER)
        go_button.pack(anchor=tkinter.CENTER)

    def setup_infos(self, username, nb_player):
        dicktionary = {
            "username": username.get(),
            "nb_player": nb_player.get()
        }
        # network = Network.Network('192.168.232.13', 5555)
        # network.connect()
        # network.send(pickle.dumps(dicktionary))
        # print(network.client.recv(200).decode())

        ToFile("assets/config.json", "json", dicktionary["username"], "local_username")
        ToFile("assets/config.json", "json", dicktionary["nb_player"], "nb_users_asked")
        for widget in self.master.winfo_children():
            widget.destroy()
        self.master.switch_frame(Game)

class WaitingRoom(tkinter.Frame):
    def __init__(self, master):
        print("Connecté et en attente d'autres joueurs")
        tkinter.Frame.__init__(self, master)

        self.pack(fill=tkinter.BOTH, expand=1)
        title = tkinter.Label(master, text="Veuillez patienter", width=20)

        local_username = ReadFile("assets/config.json", "json", "local_username")
        nb_player_max = ReadFile("assets/config.json", "json", "nb_users_asked")
        list_of_players = [local_username]
        dump_list = []
        for nb in range(1, nb_player_max, 1):
            dump_list.append("Player{}".format(nb+1))
        print("""
        Gloabal infos:
        Local Username: {0}
        Nb player asked: {1}
        """.format(local_username, nb_player_max))
        print("Longueur list_of_player:", len(list_of_players))
        print("Contenu de dump_list", dump_list)

        i = 0
        while len(list_of_players) < nb_player_max:
            if i < len(list_of_players):
                tkinter.Label(master, text="{}\n".format(list_of_players[i])).pack()
            elif i > len(list_of_players):
                # tkinter.Label(master, text=dump_list[i]).pack(anchor=tkinter.CENTER)
                break
            i += 1

        exit_button = tkinter.Button(self, text="Quitter", command=lambda: master.switch_frame(QuitGame), width=20,
                                     relief=tkinter.FLAT)


        title.pack(anchor=tkinter.CENTER)
        exit_button.pack(anchor=tkinter.CENTER)


class Rules(tkinter.Frame):
    def __init__(self, master):
        print("Afficher les règles")
        tkinter.Frame.__init__(self, master)

        rule_file = open("assets/regles.txt", 'r')
        content = rule_file.read()
        rule_file.close()
        root_text = tkinter.Text(self, height=25, width=100, wrap=tkinter.WORD)
        root_text.insert(tkinter.INSERT, content)
        root_text.pack(side=tkinter.TOP)
        go_button = tkinter.Button(self, text="Commencer", command=lambda: master.switch_frame(ConnectionForm), width=20,
                                   relief=tkinter.FLAT)
        go_button.pack()
        exit_button = tkinter.Button(self, text="Quitter", command=lambda: master.switch_frame(QuitGame), width=20,
                                     relief=tkinter.FLAT)
        exit_button.pack()


class QuitGame:
    def __init__(self, master):
        print("Exit Game...")
        exit()


if __name__ == '__main__':
    app = GlobalApp()
    app.mainloop()
